<?php

/**
 * @file
 * Integration with Dokeos
 */

// Base URL
define('DOKEOS_BASE_URL', '/main/webservices');
// Base API KEY URL
define('DOKEOS_API_KEY_URL', '/main/auth/profile.php');
// Default amount of items(photos) per page.
define('DOKEOS_DEFAULT_IPP', 10);
// signature types
define('DOKEOS_GLOBAL_SIGNATURE', 1);
define('DOKEOS_PERUSER_SIGNATURE', 2);

/**
 * Implementation of hook_init().
 */
function dokeos_init() {
  global $user;
  // Implementation of Master SSO server
  if ($user->uid && isset($_GET['sso_referer']) && variable_get('dokeos_appkey', '') != '') {
    dokeos_sso_auth($user);
  }
}

function dokeos_sso_protocol() {
  $protocols = array('http://', 'https://');
  return $protocols[variable_get('dokeos_sso_protocol', 0)];
}

/**
 * Given a drupal account, redirect to Vanity server including login info.
 */
function dokeos_sso_auth($account) {
  global $base_path;
  $master_auth_uri = filter_xss($_SERVER['HTTP_HOST']) . $base_path . '?q=user';
  // Master Cookie
  $sso = array(
    'username' => $account->name,
    'secret' => sha1(variable_get('dokeos_appkey', '')),
    'master_domain' => filter_xss($_SERVER['HTTP_HOST']),
    'master_auth_uri' => $master_auth_uri,
    'lifetime' => time() + 3600,
    'target' => filter_xss($_GET['sso_target']),
  );
  $cookie = base64_encode(serialize($sso));

  // Redirect to Vanity Server
  $url = dokeos_sso_protocol() . $master_auth_uri;
  $params = 'sso_referer=' . urlencode($url) . '&sso_cookie=' . urlencode($cookie);
  header('Location: ' . filter_xss($_GET['sso_referer']) . '?' . $params);
  ?><html>
<head><title><?php print t('Redirection');?></title>
</head><body><?php print t('Please wait...');?></body></html><?php
  exit;
}

function dokeos_soap_call() {
  // Prepare params
  $params = func_get_args();
  $service = array_shift($params);
  $action = array_shift($params);
  ini_set('soap.wsdl_cache_enabled', 0);
  // Init SOAP client
  $client = new SoapClient(dokeos_get_service_path($service));
  $dokeos_user = variable_get('dokeos_user', '');
  $signature = dokeos_get_signature();
  // Make call and its return result
  return call_user_method_array($action, $client, array_merge(array($dokeos_user, $signature), $params));
}

/**
 * Implementation of hook_user().
 */
function dokeos_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'load':
      static $dokeos_accounts;

      if (!isset($dokeos_accounts)) {
        $dokeos_accounts = variable_get('dokeos_accounts', array());
      }
      if (!isset($account->dokeos_user)) {
        $account->dokeos_user = in_array($account->uid, $dokeos_accounts);
      }
      dokeos_load_user_data($account);

      break;
  }
}

/**
 * Implementation of hook_user_login().
 */
function dokeos_user_login(&$edit, &$account, $category = NULL) {
  if ($account->uid && isset($_GET['sso_referer'])) {
    dokeos_sso_auth($account);
  }
}

/**
 * Implementation of hook_user_update().
 */
function dokeos_user_update(&$edit, &$account, $category = NULL) {
  $admin_access = user_access('administer dokeos');
  $connect_access = user_access('connect with own Dokeos account');
  $sync_access = user_access('sync users with Dokeos accounts');
  if ($category == 'account') {
    if (!isset($edit['dokeos_settings']['sync_account']) && ($sync_access || $admin_access)) {
      $users = variable_get('dokeos_accounts', array());
      // Sync account
      if ($edit['dokeos_settings']['sync_account']) {
        // Check as dokeos account
        $dokeos_account = dokeos_soap_call('user_manager', 'DokeosWSUserInfoFromUsername', array('name' => $edit['name']));
        // Deterimine wether to Create or Override account at dokeos
        if (isset($dokeos_account->firstName) ? $dokeos_account->username == '' : FALSE) {
          $action = 'DokeosWSCreateUser';
        }
        else {
          $action = 'DokeosWSUpdateUser';
        }
        // Make call
        $user = dokeos_soap_call('user_manager', $action, array(
          'loginname' => $edit['name'], 'password' => ($edit['pass'] ? $edit['pass'] : $account->pass),
          'password_crypted' => 1, 'email' => $edit['mail'],
        ));
        // Save
        if (!isset($users[$account->uid])) {
          $users[$account->uid] = $account->uid;
          variable_set('dokeos_accounts', $users);
        }
        drupal_set_message('User succesfully syncronized with Dokeos.');
      }
      // Unlink account
      else {
        // Save
        if (isset($users[$account->uid])) {
          unset($users[$account->uid]);
          variable_set('dokeos_accounts', $users);
        }
      }
    }
    if ($connect_access || $admin_access) {
      dokeos_save_user_data($edit, $account, $category);
    }
  }
}
/**
 * Implementation of hook_user_form().
 */
function dokeos_user_form(&$edit, &$account, $category = NULL) {
  dokeos_load_user_data($account);
  $admin_access = user_access('administer dokeos');
  $connect_access = user_access('connect with own Dokeos account');
  $sync_access = user_access('sync users with Dokeos accounts');
  if ($category == 'account' && ($connect_access || $sync_access || $admin_access)) {
    $form['dokeos_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dokeos settings'),
      '#collapsible' => TRUE,
      '#weight' => 1,
      '#tree' => TRUE,
    );
    if ($sync_access || $admin_access) {
      $form['dokeos_settings']['sync_account'] = array(
        '#type' => 'checkbox',
        '#title' => t('Sync with a Dokeos account.'),
        '#default_value' => in_array($account->uid, variable_get('dokeos_accounts', array())),
        '#description' => t('If enabled, system will create or override a Dokeos account with same username, email and password.')
      );
    }
    if ($connect_access || $admin_access) {
      $form['dokeos_settings']['duser'] = array(
        '#type' => 'textfield',
        '#title' => t('Dokeos username'),
        '#default_value' => $account->dokeos_settings['user'],
        '#description' => t('Dokeos username.'),
      );

      $api_key_url = dokeos_sso_protocol() . variable_get('dokeos_server', '') . DOKEOS_API_KEY_URL;

      $form['dokeos_settings']['apikey'] = array(
        '#type' => 'textfield',
        '#title' => t('Dokeos API key'),
        '#default_value' => $account->dokeos_settings['apikey'],
        '#description' => t('Dokeos API key. Find your API key in your Dokeos Profile by clicking <a href="@url">here</a>.',array('@url' => $api_key_url)),
      );
      $form['dokeos_settings']['course_visibility'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Trainings Visibilities'),
        '#description' => t('If this site displays a block with a list of your courses, will be filtered by criterias above.'),
        '#default_value' => !empty($account->dokeos_settings['course_visibility'])? $account->dokeos_settings['course_visibility']: array(),
        '#options' => dokeos_course_visibility(),
      );
      $form['dokeos_settings']['agenda_time_frame'] = array(
        '#type' => 'radios',
        '#title' => t('Agenda time frame'),
        '#default_value' => $account->dokeos_settings['agenda_time_frame'],
        '#options' => dokeos_agenda_time_frame(TRUE),
      );
    }
    return $form;
  }
}

function dokeos_load_user_data(&$account) {
  $result = db_query('SELECT duser, apikey, course_visibility, agenda_time_frame FROM {dokeos_user} WHERE uid = (:uid)', array(':uid' => $account->uid));

  foreach($result as $record) {
    $account->dokeos_settings = array(
      'user' => $record->duser,
      'apikey' => $record->apikey,
      'course_visibility' => unserialize($record->course_visibility),
      'agenda_time_frame' => $record->agenda_time_frame,
    );
    return;
  }

  $account->dokeos_settings = array(
      'user' => '',
      'apikey' => '',
      'course_visibility' => '',
      'agenda_time_frame' => '',
  );
}

function dokeos_save_user_data(&$edit, &$account, $category, $register = FALSE) {
  $data = &$edit['dokeos_settings'];

  // Pre-process courses visibility
  $data['course_visibility'] = preg_grep('/^0$/', $data['course_visibility'], PREG_GREP_INVERT);
  $data['course_visibility'] = count($data['course_visibility']) ? serialize($data['course_visibility']) : NULL;
  
  // Pre-process agenta time frame
  if ($data['agenda_time_frame'] == '0') {
    $data['agenda_time_frame'] = '';
  }
  // Look for data
  $save_data = FALSE;
  $fields = array(NULL, 'duser', 'apikey', 'course_visibility', 'agenda_time_frame');
  while($field = next($fields)) {
    if (!empty($data[$field]) && $data[$field]) {
      $save_data = TRUE;
      break;
    }
  }

  // Delete old user data
  db_delete('dokeos_user')
    ->condition('uid', $account->uid)
    ->execute();

  if ($save_data) {
    $edit = (object)array(
      'uid' => $account->uid,
      'duser' => $data['duser'],
      'apikey' => $data['apikey'],
      'course_visibility' => $data['course_visibility'],
      'agenda_time_frame' => $data['agenda_time_frame'],
    );
    drupal_write_record('dokeos_user', $edit);
  }
}

/**
 * Implementation of hook_menu().
 */
function dokeos_menu() {
  $items['admin/settings/dokeos'] = array(
    'title' => 'Dokeos',
    'description' => 'Configure integration settings with Dokeos.',
    'page callback' => 'dokeos_admin_settings',
    'access callback' => 'user_access',
    'access arguments' => array('administer dokeos'),
    'file' => 'dokeos.admin.inc',
  );
  return $items;
}

function dokeos_get_signature($type = DOKEOS_GLOBAL_SIGNATURE) {
  global $user;

  switch ($type) {
    case DOKEOS_PERUSER_SIGNATURE:
      dokeos_load_user_data($user);
      if (isset($user->dokeos_settings)) {
        return sha1($user->dokeos_settings['user'] . $user->dokeos_settings['apikey']);
      }
      break;
    default:
    case DOKEOS_GLOBAL_SIGNATURE:
      $dokeos_user = variable_get('dokeos_user', '');
      $dokeos_apikey = variable_get('dokeos_appkey', '');
      return sha1($dokeos_user . $dokeos_apikey);
  }
}

/**
 * Implementation of hook_block_list().
 */
function dokeos_block_list() {
  $blocks['trainings'] = array(
    'info' => t('Dokeos trainings'),
    'weight' => 0,
  );
  $blocks['user_agenda'] = array(
    'info' => t('My Dokeos agenda'),
    'weight' => 0,
  );
  $blocks['user_trainings'] = array(
    'info' => t('My Dokeos trainings'),
    'weight' => 0,
  );
  return $blocks;
}

/**
 * Implementation of hook_block_view().
 *
 * Generates the administrator-defined blocks for display.
 */
function dokeos_block_view($delta = '') {
  global $user;

  if (variable_get('dokeos_server', '') == '') {
    drupal_set_message(t("Dokeos module yet not configured, can't display blocks."));
    return;
  }

  $block = array();

  switch ($delta) {
    case 'trainings':
      $signature = dokeos_get_signature();
      $dokeos_user = variable_get('dokeos_user', '');
      $service = dokeos_get_service_path('courses_list');
      $client = new SoapClient($service);
      $dokeos_courses_block_items = variable_get('dokeos_courses_block_items', array('public'));
      foreach ($dokeos_courses_block_items as $key => $item) {
        if ($item === 0) {
          unset($dokeos_courses_block_items[$key]);
        }
      }
      $visibilities = implode(',', $dokeos_courses_block_items);
      $visibilities = 'public-registered';
      $courses = $client->DokeosWSCourseList($dokeos_user, $signature, $visibilities);
      $block = array(
        'subject' => t('Dokeos trainings'),
        'content' => theme('dokeos_course_list', $courses),
      );
      break;
    case 'user_agenda':
      if (isset($user->dokeos_settings)) {
        $signature = dokeos_get_signature(DOKEOS_PERUSER_SIGNATURE);
        $service = dokeos_get_service_path('user_info');
        $agenda_time_frame = !empty($user->dokeos_settings['agenda_time_frame'])? $user->dokeos_settings['agenda_time_frame']: 30;
        $datestart = (int) date('Ymd');
        $dateend = (int) date('Ymd', strtotime("+$agenda_time_frame days"));
        $client = new SoapClient($service);
        $agenda = $client->DokeosWSEventsList($user->dokeos_settings['user'], $signature, $datestart, $dateend);
        $block = array(
          'subject' => t('My Dokeos agenda'),
          'content' => theme('dokeos_user_agenda_list', $agenda),
        );
      }
      break;
    case 'user_trainings':
      if (isset($user->dokeos_settings)) {
        $signature = dokeos_get_signature(DOKEOS_PERUSER_SIGNATURE);
        $service = dokeos_get_service_path('user_info');
        $client = new SoapClient($service);
        $agenda = $client->DokeosWSCourseListOfUser($user->dokeos_settings['user'], $signature);
        $data['content'] = theme_dokeos_course_list($agenda);
        $block = array(
          'subject' => t('My Dokeos trainings'),
          'content' => theme('dokeos_course_list', $agenda),
        );
      }
      break;
  }
  return $block;
}

/**
 * Implementation of hook_block_configure().
 */
function dokeos_block_configure($delta = '') {
  if ($delta == 'trainings') {
    $form['items'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Trainings Visibilities'),
      '#default_value' => variable_get('dokeos_courses_block_items', array()),
      '#options' => dokeos_course_visibility(),
    );
    return $form;
  }
}

/**
 * Implementation of hook_block_save().
 */
function dokeos_block_save($delta = '') {
  if ($delta == 'trainings') {
    variable_set('dokeos_courses_block_items', $edit['items']);
  }
}

/**
 * Implementation of hook_theme().
 */
function dokeos_theme($existing, $type, $theme, $path) {
  return array(
    'dokeos_course_list' => array(
      'arguments' => array('courses' => NULL),
    ),
    'dokeos_user_agenda_list' => array(
      'arguments' => array('agenda_items' => NULL),
    ),
    'dokeos_course_list' => array(
      'arguments' => array('courses' => NULL),
    ),
  );
}

/**
 * Prepare courses list
 * @param   Array
 *    A list of course details as returned by dokeos_curl_exec()
 */
function theme_dokeos_course_list($courses) {
  if (!is_array($courses)) return;
  if (count($courses)) {
    drupal_add_css(drupal_get_path('module', 'dokeos') . '/dokeos.css', 'module', 'all', FALSE);
    ob_start();
    ?><ul><?php
    foreach ($courses as $course) {
?><li class="dokeos-course">
<span class="dokeos-course-title"><a href="<?php print $course->url ?>"><?php print $course->title ?></a></span>
<span class="dokeos-course-info"><?php print $course->language . ' - ' . $course->teacher?></span>
</li><?php
    }
    ?></ul><?php
    $output = ob_get_contents(); ob_end_clean();
    return $output;
  }
}

function theme_dokeos_user_agenda_list($agenda_items) {
  if (!is_array($agenda_items)) return;
  $path = drupal_get_path('module', 'dokeos');
  drupal_add_css($path . '/dokeos.css', 'module', 'all', FALSE);
  $output = '';
  $agenda = array();
  foreach ($agenda_items as $item) {
    $html  = '<span class="dokeos-event-title">' . l($item->title, $item->link) . '</span>';
    $html .= '<span class="dokeos-event-info">' . $item->coursetitle . '</span>';
    $html .= '<span class="dokeos-event-info">' . date('M j, Y, g:i a', strtotime($item->datestart)) . '</span>';
    $html .= '<span class="dokeos-event-info">' . date('M j, Y, g:i a', strtotime($item->dateend)) . '</span>';
    $agenda[] = $html;
  }
  $output .= theme('item_list', $agenda);
  return $output;
}

/**
 * Implementation of hook_perm().
 */
function dokeos_perm() {
  return array(
    'administer dokeos' => array(
      'title' => t('Administer Dokeos'),
      'description' => t('Configure Dokeos integration settings.'),
    ),
    'connect with own Dokeos account' => array(
      'title' => t('connect with own Dokeos account'),
      'description' => t('Access to SSO and user blocks.'),
    ),
  );
}

/**
 * Return supported course visibility options
 */
function dokeos_course_visibility() {
  return array(
    'public' => t('public'),
    'private' => t('private'),
    'public-registered' => t('public registered'),
    'closed' => t('closed')
  );
}

function dokeos_agenda_time_frame($optional = FALSE) {
  static $time_frames;

  if (!is_array($time_frames)) {
    $time_keys = array(30 => '', 15 => '', 7 => '');
    // Describe time frames
    $time_frames = $optional ? array('0' => t('Disabled')) : array();
    foreach ($time_keys as $f => $d) {
      $time_frames[$f] = t('between now and within next !n days', array('!n' => $f));
    }
  }

  return $time_frames;
}

/**
 * Given a $service name, return corresponding location and uri into an array.
 */
function dokeos_get_service_path($service) {
  static $services;
  $dokeos_server = variable_get('dokeos_server', '');
  if (!isset($services)) {
    $services = array('courses_list', 'user_info');
  }
  if (in_array($service, $services)) {
    return dokeos_sso_protocol(). $dokeos_server . DOKEOS_BASE_URL . '/' . $service . '.soap.php?wsdl';
  }
}
